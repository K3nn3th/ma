# cSpell:disable */
#Untersuchung zur Extraktion des Instrumentals aus mehreren Musikstücken mit gleichem Instrumental

#- Separierung mit Hilfe von spleeter -> anschl. Vergleich der Ergebnisse und "Korrektur"
# anh. von Wahrscheilichkeiten
#- Training mit instrumentals - vocals mix
#- Alignment/Warping of Instrumentals
import sys, os
from spleeter.separator import Separator

import multiprocessing
import multiprocess
#from multiprocess import Pool
import concurrent.futures
from functools import partial

import librosa
import librosa.display
from collections import Counter
from matplotlib import pyplot as plt
from matplotlib import cm
#plt.style.use('ggplot')

from matplotlib.lines import Line2D
from matplotlib.colors import ListedColormap

import numpy as np
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map  # or thread_map
import scipy.ndimage
from scipy import stats
import time
import subprocess

#from kde_diffusion import kde1d
from pyts.metrics import dtw as dtw_pyts
from fastdtw import fastdtw
from dtw import dtw as dtw2
from scipy.spatial.distance import euclidean

import statistics
import soundfile as sf

#import q
import pandas
from varname import nameof
from palettable.scientific.diverging import Berlin_20
# cSpell:enable */

# GLOBALS

## when True, unspleeted tracks will be used instead of the spleeted accompaniments
TRACKS = False

REGENERATE_PATHS = True
REGENERATE_SPECTOS = True
SHOW_FIG = False
power = 11
n_fft = 2**power
hop_length = n_fft//8
# length of desired audio, None for entire track
seconds = 25
seconds_total = 40

# percent of ground truth instrumental frequency amplitudes outside of available data (greater than max or smaller than min)
percent_outliers = 0

# Without Instrumentals:
#folder_name = 'Fire Avenue Riddim'
#folder_name = 'Laba Laba Riddim'   #  nice.

#folder_name = 'GFH'
#folder_name = 'Rankin Skankin Riddim'

# With Instrumentals:
#folder_name = 'Applause Riddim'
#folder_name = 'BBQ Riddim'
#folder_name = 'Baddis Ting Riddim'
#folder_name = 'Doctors Darling Riddim'
#folder_name = 'Prisonbreak Riddim'
#folder_name = 'Hardtimes Riddim'   # 'phasy' and jittery
#folder_name = 'Liquid Riddim'     # ParameterError: DTW cost matrix C has NaN values. ?
#folder_name = 'Reggae Fest Riddim'
#folder_name = 'Reggae Revolution Riddim'
#folder_name = 'Reggae Pickney Riddim'
#folder_name = 'Wire Dem Riddim'
#folder_name = 'Eye Charm Riddim'
folder_name = 'Aidia Riddim'
#folder_name = 'Styling Gel Riddim'

# for testing index correctness
#folder_name = 'General Test'
#folder_name = 'test_index'
#folder_name = 'test_alignment'
#folder_name = 'DEBUG'


#base_path = os.path.abspath('D:\\UNI\\MA\\data')
#base_path = os.path.abspath('/home/archm4n/Documents/UNI/ma/data')
base_path = os.path.abspath('/run/media/engineer/DATA/UNI/MA/data')

riddims = os.listdir(base_path)
for a in sys.argv:
    if a in riddims:
        folder_name = a

track_folder_path = os.path.join(base_path, folder_name)
path_split = os.path.join(track_folder_path, 'split')
path_split_16 = os.path.join(track_folder_path, 'split_16')
path_split_demucs = os.path.join(track_folder_path, 'split_demucs')
path_specto = os.path.join(track_folder_path, 'specto')
path_paths = os.path.join(track_folder_path, 'paths')
path_graphs = os.path.join(track_folder_path, 'graphs')
path_split_griffinlim = os.path.join(track_folder_path, 'split_griffinlim')
path_results = os.path.join(track_folder_path, 'results')
path_comparison = os.path.join(track_folder_path, 'comparison')
#path_split = os.path.join(track_folder_path, 'split_new')

if not os.path.exists(path_split_griffinlim):
    os.mkdir(path_split_griffinlim)
if not os.path.exists(path_specto):
    os.mkdir(path_specto)
if not os.path.exists(path_paths):
    os.mkdir(path_paths)
if not os.path.exists(path_graphs):
    os.mkdir(path_graphs)
if not os.path.exists(path_results):
    os.mkdir(path_results)
if not os.path.exists(path_comparison):
    os.mkdir(path_comparison)

result_tracks = None
result_instrumentals = None


class Track:    
    def __init__(self, name, filepath=None):
        self.name = name
        self.filepath = filepath
        self.offsets = {}
        self.warp_paths = {}
        self.warp_paths_rough = {}
        #self.ds = {}
        self.specto = None#self.generate_specto()
        self.phase = None
        self.total_curvature = None  
        self.positive_cs = 0
        self.sr = 0
        self.cutoff = 0

    # handle (-)inf case ?
    def get_all_curvatures(self):
        total = 0
        for path in self.warp_paths:
            c = get_curvature_for_path(self.warp_paths_rough[path])
            print(path + 's curvature: ' + str(c))
            total += c
        self.total_curvature = total# / len(self.warp_paths)
        print(self.name + ' total_curvature: ' + str(self.total_curvature))        
        
    def smooth_paths(self, kernel_size):
        for p in self.warp_paths:
            #self.warp_path[p] = 
            D = 0
            plot_wp(None, self.warp_paths[p], p, self.name, self.sr, with_D=False)
            #plot_wp(self.ds[p], self.warp_paths[p], p, self.name, self.sr)
            new = smooth_path(self.warp_paths[p], kernel_size)
            plot_wp(None, new, p + ' - smooth', self.name + ' - smooth', self.sr, with_D=False)
            #plot_wp(self.ds[p], new, p + ' - smooth', self.name + ' - smooth', self.sr)#, False)

    
    # takes track to compare
    def generate_path(self, track_compare):
        global tracks_spleeter
        t_compare = track_compare.name
        print('generating path for: ' + self.name + ' / ' + t_compare)
        path_filename = self.name + '_' + t_compare
        path_warping_path = os.path.join(path_paths, path_filename)

        # if the spleeted version already has a path for that track then use it
        #if tracks_spleeted[self.name].warp_paths[t_compare] != None:
        success = True
        try:
            wp_n = tracks_spleeter[self.name].warp_paths[t_compare]
            c = tracks_spleeter[self.name].offsets[t_compare]
            #c = get_best_fit_c(wp_n)
            self.offsets[t_compare] = c
            #self.warp_paths[t_compare] = wp_n
            path_info = np.asarray((wp_n, c))
            print('found previously calculated path in spleeter results')
            return path_info
        except:
            success = False

        if not success and (not os.path.exists(path_warping_path) or REGENERATE_PATHS):
            if '-debug' in sys.argv:
                print('couldnt find previously calculated path')
            global tracks
            t_specto = self.specto[:self.cutoff]
            t_compare_specto = track_compare.specto[:track_compare.cutoff]
            
            # D and wp are numpy arrays
            #start = time.time()

            if '-mfcc' in sys.argv:

                mfcc1 = librosa.feature.mfcc(S=self.specto[:seconds * self.sr].T)
                print('got first mel spec')
                mfcc2 = librosa.feature.mfcc(S=track_compare.specto[:seconds * self.sr].T)
                print('got second mel spec')

#dist, cost, path = dtw(mfcc1.T, mfcc2.T)
                #dist, cost, acc_cost, path = dtw2(mfcc1.T, mfcc2.T, dist=lambda x, y: np.linalg.norm(x - y, ord=1))
                distance, wp = fastdtw(mfcc1.T, mfcc2.T, dist=euclidean, radius=4)

                print('got path')
                import ipdb; ipdb.set_trace(context=11)


            elif '-fastdtw' in sys.argv:
                '''
                start = time.time()
                distance, wp = fastdtw(t_specto.T, t_compare_specto.T, dist=euclidean)
                wp.reverse()
                wp = np.asarray(wp)
                D = None
                with_d = False
                '''
                mid = time.time()
                distance, wp = fastdtw(t_specto, t_compare_specto, dist=euclidean, radius=4)
                wp.reverse()
                wp = np.asarray(wp)
                D = None
                with_d = False
                end = time.time()
                #print('fastdtw took : ' + str(end-mid))
                #print('fastdtw took : ' + str(mid-start) + ' with radius 4: ' + str(end-mid))
            else:
                D, wp = librosa.sequence.dtw(X=t_specto, Y=t_compare_specto, metric='euclidean')
                with_d = True
            if '-debug' in sys.argv:
                print('done with path calculation. determining offset now.')

            #import ipdb; ipdb.set_trace(context=11)

            #dist, cost_mat, acc_cost_mat, path = dtw_pyts(t_specto.tolist(), t_compare_specto.tolist(), method='sakoechiba', options={'window_size': 0.5})
            #mid2 = time.time()
            #alignment = dtw(t_specto, t_compare_specto)
            #dist, cost_mat, acc_cost_mat, path = dtw_pyts(t_specto.tolist(), t_compare_specto.tolist(), method='fast', options={'radius': 1})
            #end = time.time()
            #print('librosa dtw took: ' + str(mid - start) + ' vs:  (fast)' + str(mid2 - mid) + ' and (dtw-python) ' + str(end - mid2))
            #import ipdb; ipdb.set_trace(context=11)

            wp_only_slope = only_slope(wp)
            c = get_best_fit_c(wp_only_slope)
            #c = get_best_fit_c(wp)
            wp_n = None

            # generating the line points
            if c != None:
                # take first instance of last value of wp as length value

                #print('First value pair of calculated path: ' + str(wp[0]))
                #print('Last x value of calculated path: ' + str(wp[-1]))

                highest_x = wp[0][1]
                wp_n = []
                for x in range(highest_x):
                    if x >= 0 and x + c >= 0:
                        wp_n.append((x, x + c))
            
            wp_n = np.asarray(wp_n)

            fig = plot_wp(D, wp, self.name, t_compare, self.sr, c=c, wp_n=wp_n, wp_slope=wp_only_slope, with_D=with_d)
            
            # if graph for t with t_compare doesnt exist, write it
            path_plot = os.path.join(path_graphs, self.name + '++' + t_compare + '.png')            
            if not os.path.exists(path_plot):
                plt.savefig(path_plot)
            if not SHOW_FIG:
                plt.close(fig)
            if '-debug' in sys.argv:
                print('got offset and plotted path')
                
            self.warp_paths_rough[t_compare] = wp[::-1]
            self.warp_paths[t_compare] = wp_n

            print('Generated path for: ' + self.name + '|' + t_compare + ' length: ' + str(len(self.warp_paths[t_compare])) + ' - c value: ' + str(c))
            #print('with curvature: ' + str(curv))
            #print('start looks like this: ' + str(self.warp_paths[t_compare][:20]))

            path_info = np.array((wp_n, c), dtype=object)
            np.save(path_warping_path, path_info)
        else:
            path_info = np.load(path_warping_path)

        return path_info#(wp_n, c)

# returns the parts of the path with a certain slope
def only_slope(path, slope=1, min_length=20):
    path_only_slope = []
    #import ipdb; ipdb.set_trace(context=11)
    for i, coord in enumerate(path[min_length:]):
        diff_x = path[i - min_length][0] - path[i][0]
        diff_y = path[i - min_length][1] - path[i][1]
        if diff_x == min_length and diff_y == min_length * slope:
            path_only_slope.append(path[i - min_length])

    return np.asarray(path_only_slope)


def generate_paths_threaded(t):#, _tracks):
    #tracks = _tracks
    global tracks
    print('')
    start = time.time()
    total = 0
    other_tracks = dict(tracks)
    del other_tracks[t]
    try:
        del other_tracks['_name']
    except:
        print('no key named _name!')
    #print(t + ' and others: ' + str(other_tracks))
    other_tracks_vals = list(other_tracks.values())
    print('starting multiprocessed path generation')
    with multiprocess.Pool(os.cpu_count() - 1) as pool:
        paths_cs = pool.map(tracks[t].generate_path, other_tracks_vals)
    print('ended multiprocessed path generation')
    #for t_compare in tracks:
    #    # optimisation: only compare ones up to current index and add flipped path to other track
    #    if t_compare != t:
    #        # generate_path returns the curvature besides generating the path 
    #        total += generate_path(t, t_compare)
    #tracks[t].total_curvature = curvatures[0]# / len(self.warp_paths)
    end = time.time()
    print('generating paths took: ' + str(end - start))
    min_c = 0
    #for p in paths_cs:
    #    if p[1] < min_c:
    #        min_c = p[1]
    #min_c = 0
    min_c_tr = t
    #print(min_c_tr + ' has c value: ' + str(min_c))
    for i, other_track in enumerate(other_tracks_vals):
        others_path = paths_cs[i]
        tracks[t].warp_paths[other_track.name] = others_path[0]
        tracks[t].offsets[other_track.name] = others_path[1]
        if others_path[1] < min_c and other_track.name != folder_name:
            min_c = others_path[1]
            min_c_tr = other_track.name
    print(min_c_tr + ' has smallest c value: ' + str(min_c))
    #    tracks[tr.name] = tr
    #print(t + ' generated ' + str(len(tracks[t].warp_paths)) + ' warp paths')
    #if min_c_tr != t:
    #    min_c_tr = generate_paths_threaded(min_cr_t)
    return min_c_tr

def generate_paths(t):#, _tracks):
    #tracks = _tracks
    start = time.time()
    #total = 0
    smallest_c = 0#sys.maxsize
    smallest_c_name = t
    for t_compare in tracks:
        # optimisation: only compare ones up to current index and add flipped path to other track
        if t_compare != t and t_compare != '_name':
            # generate_path returns the curvature besides generating the path 
            
            path_and_c = tracks[t].generate_path(tracks[t_compare])
            #print('path and c type: ' + str(type(path_and_c)))
            tracks[t].warp_paths[t_compare] = path_and_c[0]
            tracks[t].offsets[t_compare] = path_and_c[1]
            if path_and_c[1] < smallest_c:
                smallest_c = path_and_c[1]
                smallest_c_name = t_compare
    #tracks[t].total_curvature = total# / len(self.warp_paths)
    end = time.time()
    print('generating paths took: ' + str(end - start))
    print(t + ' generated ' + str(len(tracks[t].warp_paths)) + ' warp paths')
    print('smalles c (prototype) was: ' + smallest_c_name)
    return smallest_c_name
         
def plot_wp(D, wp, x_name, y_name, sr, wp_n, wp_slope, with_D = True, c=''):
    wp_s = wp#np.asarray(wp) * hop_length / sr
    wp_ns = wp_n#np.asarray(wp_n) * hop_length / sr
    wp_slope_s = wp_slope #np.asarray(wp_slope) * hop_length / sr
    #print('length wp: ' + str(len(wp)))
    #print('length wp_s: ' + str(len(wp_s)))
    
    fig = plt.figure(figsize=(12, 10))
    # make pixels square
    ax = fig.add_subplot(111)
    if with_D:
        librosa.display.specshow(D, x_axis='time', y_axis='time',
                                 cmap='magma', hop_length=hop_length, sr=sr, vmin=0, vmax=500)
        #imax = ax.imshow(D, cmap=plt.get_cmap('magma'),
        #                origin='lower', interpolation='nearest', aspect='equal')
        plt.colorbar()

    ax.plot(wp_s[:, 0], wp_s[:, 1], marker='.', color='r', ms=5, label='dtw result')
    ax.plot(wp_slope_s[:, 0], wp_slope_s[:, 1], marker='.', color='b', ms=2, label='dtw result - only slopes', linewidth=0)
    ax.plot(wp_ns[:, 0], wp_ns[:, 1], marker='.', color='g', ms=1, label='approximated')
    plt.yscale('linear')
    #print('length wp_n: ' + str(len(wp_n)))
    #print('length wp_ns: ' + str(len(wp_ns)))
    
    ax.set(xlabel=x_name + ' [windows]', ylabel=y_name + ' [windows]')
    plt.title(f'Warping Path for {x_name} and {y_name} - offset: {c}')
    return fig
    
def plot_candidates(candidates, truth, max_id, min_id=None, _range=None, approx=None):
    df_L = pandas.DataFrame(np.sort(candidates, axis=0)).T
    desc_L = df_L.T.describe().T
    df_L['INSTR'] = truth
    desc_L['INSTR'] = truth
    

    fig = plt.figure(figsize=(20,10))
    # make pixels square
    ax = fig.add_subplot(111)
    lw = .8
    
    #main_ax.set_xlabel('time (s)')
    #main_ax.set_ylabel('current (nA)')
    title = 'Candidates at spectogram[' + str(max_id) + ']'
    if _range != None:
        # import ipdb; ipdb.set_trace(context=11)
        title += ' for freq bins ' + str(_range[0]) + ' to ' + str(_range[1] - 1)
        ax.set_xticks([r for r in range(len(candidates[0]))])
        ax.set_xticklabels([str(r) for r in range(_range[0], _range[1])])

    ax.set(xlabel='Frequency bins', ylabel='Magnitude')
    ax.plot(candidates.T, marker='.', color='grey', ms=1, linewidth=lw)
    ax.plot(np.mean(candidates, axis=0).T, marker='.', color='black', ms=1, linewidth=lw)
    ax.plot(np.median(candidates, axis=0).T, marker='.', color='purple', ms=1, linewidth=lw)
    ax.plot(truth, marker='.', color='green', ms=1, linewidth=lw)
        
    ## markers / records
    legend_elements = [Line2D([0], [0], marker='o', color='grey', label='candidates', markerfacecolor='grey', markersize=5),
                        Line2D([0], [0], marker='o', color='black', label='mean', markerfacecolor='black', markersize=5),
                        Line2D([0], [0], marker='o', color='purple', label='median', markerfacecolor='purple', markersize=5),
                        Line2D([0], [0], marker='o', color='green', label='truth', markerfacecolor='black', markersize=5)]

    # plot legend
    plt.legend(handles=legend_elements, loc='upper right')
    if approx != None:
        ax.plot(approx, marker='.', color='blue', ms=1)

    plt.title(title)
    #plt.yscale('log')
    #plt.colorbar()
    #plt.show()
    return fig

def my_colormap():
    top = cm.get_cmap('Oranges_r', 128)
    bottom = cm.get_cmap('Blues', 128)

    newcolors = np.vstack((top(np.linspace(0, 1, 128)),
                           bottom(np.linspace(0, 1, 128))))
    return ListedColormap(newcolors, name='OrangeBlue')
        
def plot_specto(specto, name, sr, to_db = True):
    fig = plt.figure(figsize=(10,10))
    # make pixels square
    ax = fig.add_subplot(111)
    if to_db:
        specto = librosa.amplitude_to_db(specto, ref=np.max)
        #specto = librosa.power_to_db(specto, ref=np.max)
        cmap = plt.get_cmap('magma')
        im = librosa.display.specshow(specto, y_axis='linear', hop_length=hop_length, sr=sr, cmap=cmap)
        #im = librosa.display.specshow(specto, x_axis="time", y_axis='linear', hop_length=hop_length, sr=sr, cmap=cmap)
        fig.colorbar(im, ax=ax, format="%+2.f dB")
    else:
        #cmap = my_colormap()#plt.get_cmap('RdBu')
        cmap = Berlin_20.mpl_colormap
       # TODO: difference to db ? 
        ax.set_xlabel('Time (s)')
        ax.set_ylabel('Frequency bins')
        psm = ax.pcolormesh(specto, cmap=cmap, rasterized=True, vmin=-np.max(specto), vmax=np.max(specto))
        #im = librosa.display.specshow(specto, x_axis="time", y_axis='linear', hop_length=hop_length, sr=sr, cmap=cmap)
        fig.colorbar(psm, ax=ax, format="%+2.f magn diff")
    #im = librosa.display.specshow(specto, x_axis="time", y_axis='log', hop_length=hop_length, sr=sr, cmap="magma")
    plt.title(name)
    plt.yscale('linear')
    #plt.show()
    return fig
            
    
def test_plot_wp():
    plot_wp([],[(0, 0), (0, 1), (0, 2), (0, 3)])


def normalize(audio):
    max_peak = np.max(np.abs(audio))
    if '-debug' in sys.argv:
        print('max_peak: ' + str(max_peak))
    ratio = 1 / max_peak
    return audio * ratio


def generate_specto(track):
    _fp = track.filepath#os.path.join(os.path.join(path_split, self.name), 'accompaniment.wav')
    #_fp = os.path.join(track_folder_path, self.name)

    if not os.path.exists(_fp):
        print(_fp + ' does not exist. please check')
        return None
    first_start = time.time()

    sig, sr = librosa.load(_fp, sr=None, mono=True)
    sig_stereo, sr = librosa.load(_fp, sr=None, mono=False)
    
    if '-normalize' in sys.argv:
        sig_stereo = normalize(sig_stereo)
    
    #end = time.time()
    track.sr = sr
    # cutoff is the max sample of specto to use for dtw. its 20 seconds worth of spectogram
    track.cutoff = int((sr * seconds) / hop_length)

    spec_path = os.path.join(path_specto, track.name + '-2__' + str(power) )
    phase_path = os.path.join(path_specto, track.name + '-2__' + str(power) + '_phase' )

    if (not os.path.exists(spec_path) and not os.path.exists(phase_path)) or REGENERATE_SPECTOS:

        # trim silence at beginning and end to make it nicer
        #print('trimming signal from ' + str(len(sig)))
        #sig_trimmed, index = librosa.effects.trim(sig, top_db=58)
        #sig_stereo_trimmed, index = librosa.effects.trim(sig_stereo, top_db=58)
        #print('to: ' + str(len(sig_trimmed)))

        #sig_stereo_trimmed = sig_stereo

        #tuning = librosa.estimate_tuning(y=sig, sr=sr)
        #print(track_name + ' with tuning: ' + str(librosa.tuning_to_A4(tuning)))

        # DIFFERENT CHROMAGRAM / SPECTOGRAM GENERATION METHODS
        sig_L = sig_stereo[0]
        sig_R = sig_stereo[1]
        if seconds_total != None:
            sig_L = sig_stereo[0][:sr * seconds_total]
            sig_R = sig_stereo[1][:sr * seconds_total]

    #specto = librosa.feature.chroma_stft(short, sr=sr, n_fft=n_fft, hop_length=hop_length)
    #specto = librosa.feature.chroma_cqt(sig, sr=sr, hop_length=hop_length)


        #print('shortened length: ' + str(len(short)))
        D = librosa.stft(sig, n_fft=n_fft, hop_length=hop_length)
        D_L = librosa.stft(sig_L, n_fft=n_fft, hop_length=hop_length)
        D_R = librosa.stft(sig_R, n_fft=n_fft, hop_length=hop_length)
        specto = np.abs(D)
        specto_L = np.abs(D_L)
        specto_R = np.abs(D_R)
        with open(spec_path + '_L', 'w+b') as f:
            np.save(f, specto_L)
        with open(spec_path + '_R', 'w+b') as f:
            np.save(f, specto_R)
        #print('got specto in ' + str(end - start))
        phase_L = np.angle(D_L)
        phase_R = np.angle(D_R)
        with open(spec_path + '_phase_L', 'w+b') as f:
            np.save(f, phase_L)
        with open(spec_path + '_phase_R', 'w+b') as f:
            np.save(f, phase_R)
        
        #print('generating griffinlim comparison ..')
        #reconstructed = librosa.griffinlim(specto, hop_length=hop_length)
        #print('generating griffinlim comparison ..writing...')
        #sf.write(os.path.join(path_split_griffinlim, track.name + '.wav'), reconstructed, sr, subtype="PCM_24")
        #print('generating griffinlim comparison ..writing... Done')
        
        '''
        D = librosa.stft():
        This function returns a complex-valued matrix D such that

        np.abs(D[f, t]) is the magnitude of frequency bin f at frame t, and

        np.angle(D[f, t]) is the phase of frequency bin f at frame t.

    The integers t and f can be converted to physical units by means of the utility functions frames_to_sample and fft_frequencies.
        '''
    else:
        print('Using cached spectogram and phase for: ' + track.name)
        specto_L = np.load(spec_path + '_L')#[:sr * seconds]
        specto_R = np.load(spec_path + '_R')#[:sr * seconds]
    
    
    
    #specto = librosa.feature.melspectrogram(short, sr=sr)#, hop_length=hop_length)
    #y_inv = librosa.feature.inverse.mel_to_audio(specto, sr=sr)

    #display(ipd.Audio(y_inv, rate=sr))

    #specto_db = librosa.power_to_db(chroma, ref=np.max)
    #plot_chroma(specto_db, _fp, sr)
    
    # check if all specto vectors have the same length:
    #prev_length = 0
    #for vec in specto:
    #    if len(vec) != prev_length:
    #        print(track.name + ': old: ' + str(prev_length) + ' - new: ' + str(len(vec)))
    #        prev_length = len(vec)

    track.specto = specto.T
    track.specto_L = specto_L.T
    track.specto_R = specto_R.T
    track.phase_L = phase_L.T
    track.phase_R = phase_R.T
    end = time.time()
    print('loaded track/instrumental and specto for: ' + track.name + ' with sr=' + str(sr) + ' in ' + str(end - first_start))
    return track 


def get_curvatures(track):
    track.generate_paths()
    
def get_local_curvature(path, i, neighbours=1):
    if i + neighbours < len(path):
        start = i
        end = i + neighbours        
    else:
        start = i - neighbours
        end = i
    local_curvature = 0    
    for pos in range(start, end - 1):
        x = path[start][0]
        y = path[start][1]
        x_n = path[end][0]
        y_n = path[end][1]
        
        _curvature = ((y_n - y) / (x_n - x))#/ len(path)
        if str(_curvature) == '-inf':
            _curvature = -(2**12)
        if str(_curvature) == 'inf':
            _curvature = 2**12
        local_curvature += _curvature
            
    return local_curvature / neighbours# + 1
            

# return (average?) curvature per window
def get_curvature_for_path(path):
    print('path length: ' + str(len(path)))
    total_curvature = 0
    for i, point in enumerate(path[:-1]):
        #x = path[i][0]
        #y = path[i][1]
        #x_n = path[i + 1][0]
        #y_n = path[i + 1][1]
        #local_curvature = ((y_n - y) / (x_n - x))#/ len(path)
        #if str(local_curvature) == '-inf':
        #    local_curvature = -(2**12)
        #if str(local_curvature) == 'inf':
        #    local_curvature = 2**12
        #    print('inf or -inf found. cuplrits are:')
        #    print('x: ' + str(x))
        #    print('x_n: ' + str(x_n))
        #    print('y: ' + str(y))
        #    print('y_n: ' + str(y_n))
        #    print('calculation: (' + str(y_n - y) + '/' + str(x_n - x) + ')')
        total_curvature += get_local_curvature(path, i, 5) #local_curvature# / 10000
    #print('total curvature: ' + str(total_curvature))
    return total_curvature


def most_common(lst):
    return max(set(lst), key=lst.count)
    #return lst.sort()[len(lst)//2]
    #return sorted[len(lst)//2]
    

def get_best_fit_c(path, m = 1):
    x = path[:, 1]
    y = path[:, 0]

    # np.mean(x) - m * np.mean(y) | m = 1
    #c = -np.mean(y - x)# - (m * np.mean(y)) # shifted result if curve is too slopey
    c = -np.median(y - x)# - (m * np.mean(y))
    #generate_line
    #distances = []
    #for p in path:
    #distances = (x-y)/np.sqrt(2)
    #distances.append(dist)
    #distances = distances.sort()
    #med_dist = statistics.median(distances)
    
    rounded = int(round(c))
    #print('choosing ' + str(rounded) + ' over ' + str(c))
    return rounded


def separate(track_folder_path, mode='demucs'):
    # Using embedded configuration.
    if mode == 'spleeter':
        separator = Separator('spleeter:2stems-16kHz')

    for track in os.listdir(track_folder_path):#track_paths:
        if track.lower().endswith('.mp3') or track.lower().endswith('.flac'):
            _path = os.path.join(track_folder_path, track)
            if mode == 'spleeter':
                _dest = path_split_16
                # use separate_to_file to save results for future use
                # waveform, sr = audio_loader.load(os.path.join(track_folder_path, track), sample_rate=None)
                # prediction = separator.separate(waveform) # for general case        
                # track_waves.append(waveform)
                separator.separate_to_file(_path, _dest, synchronous=False)
            elif mode == 'demucs':
                _dest = path_split_demucs
                python_cmd = 'python' + str(sys.version_info[0]) + '.' + str(sys.version_info[1])
                demucs_args = [python_cmd, '-m', 'demucs.separate', '--two-stems=vocals', '-n', 'mdx_extra', '-j', str(os.cpu_count()), _path, '-o', path_split_demucs]
                subprocess.run(demucs_args)#' '.join(demucs_args), shell=True)
#                python3.8 -m demucs.separate --two-stems=vocals -n mdx_extra -j 8 track_folder_path/* -o track_folder_path/s

            print('separated: ' + _path + ' to ' + _dest)


    if mode == 'spleeter':
        separator.join()

    print('All tracks separated!')
    
#pool = concurrent.futures.ProcessPoolExecutor(max_workers=os.cpu_count())
jobs = []

def init_instrumentals(path):
    global tracks
    print('Initialising instrumentals..')
    start = time.time()
    count = 0
    track_list = []
    
    for p, fp, files in os.walk(path):
        for f in files:
            #count += 1
            #if count == 3:
            #    break
            if f == 'accompaniment.wav' or f == 'no_vocals.wav':
                _fp = os.path.join(p, f)
                track_name = os.path.basename(p)
                new_track = Track(track_name, _fp)
                
                generate_specto(new_track)
                
                tracks[track_name] = new_track
    for t in tracks.keys():
        if t != folder_name and t != '_name':
            proto_candidate = t
            break
    print('got spectos. getting paths...')
    #for track in tracks:
    #    generate_paths(track)#, tracks)
    try:
        proto_name = proto.name
        print('---> using pre-found proto: ' + proto_name)
    except:
        proto_name = generate_paths(proto_candidate)#, tracks)
    generate_paths(proto_name)#, tracks)
                
    end = time.time()
    #print(tracks)
    print('init_instrumentals.. DONE. took: ' + str(end - start) + '\n--------------\n')
    return proto_name

def init_tracks():
    start = time.time()
    print('Initialising Tracks..')
    for f in os.listdir(track_folder_path):
        _fp = os.path.join(track_folder_path, f)
        if os.path.isfile(_fp) and (f.endswith('.flac') or f.endswith('.mp3')):
            track_name = os.path.splitext(f)[0]
            new_track = Track(track_name, _fp)
            generate_specto(new_track)
            tracks[track_name] = new_track

    print('got spectos. getting paths...')
    
    try:
        proto_name = proto.name
        print('---> using pre-found proto: ' + proto_name)
    except:
        # take any track and calculate the paths to other tracks. generate_paths() also returns the track name which has the earliest matching occurence
        proto_name = generate_paths(track_name)#, tracks)
    generate_paths(proto_name)#, tracks)

    end = time.time()
    print('init_tracks.. DONE. took: ' + str(end - start) + '\n--------------\n')
    return proto_name

def init_tracks_threaded():
    global tracks
    track_list = []
    

    start = time.time()
    print('Initialising Tracks..')
    for f in os.listdir(track_folder_path):
        _fp = os.path.join(track_folder_path, f)
        if os.path.isfile(_fp) and (f.endswith('.flac') or f.endswith('.mp3')):
            track_name = os.path.splitext(f)[0]
            new_track = Track(track_name, _fp)
            track_list.append(new_track)
            #generate_specto(new_track)
            #tracks[track_name] = new_track
            if track_name != folder_name:
                proto_candidate = track_name

    with multiprocess.Pool(os.cpu_count()) as pool:
        tracks_with_spectos = pool.map(generate_specto, track_list)

    for t in tracks_with_spectos:
        tracks[t.name] = t
    print('GENERATING PATHS...\n')# + str(tracks.keys()))
    
    try:
        proto_name = proto.name
        #tracks[proto_name].warp_paths = tracks_spleeter[proto_name].warp_paths
        print('---> using pre-found proto: ' + proto_name)
        generate_paths_threaded(proto_name)
    except:
        proto_name = generate_paths_threaded(proto_candidate)
        print('found proto: ' + proto_name)
        if proto_name != proto_candidate:
            generate_paths_threaded(proto_name)
    
    # take any track and calculate the paths to other tracks. generate_paths() also returns the track name which has the earliest matching occurence

    end = time.time()
    print('init_tracks_threaded.. DONE. took: ' + str(end - start) + '\n--------------\n')
    return proto_name

        
def init_instrumentals_threaded(path):
    print('init instrumentals in: ' + str(path))
    global tracks
    start = time.time()
    count = 0
    track_list = []
    futures1 = []
    procs = []
    
    proto_candidate = list(tracks.keys())[0]
    for p, fp, files in os.walk(path):
        for f in files:
            if f == 'no_vocals.wav' or f == 'accompaniment.wav':
                _fp = os.path.join(p, f)
                
                track_name = os.path.basename(p)
                print('found accompaniment : ' + track_name)
                new_track = Track(track_name, _fp)
                track_list.append(new_track)
                if track_name != folder_name:
                    proto_candidate = track_name
            #elif mode == 'demucs' and f == 'no_vocals.wav':

    with multiprocess.Pool(os.cpu_count()) as pool:
        tracks_with_spectos = pool.map(generate_specto, track_list)

    for t in tracks_with_spectos:
        tracks[t.name] = t
    print('GENERATING PATHS...\n')# + str(tracks.keys()))
    
    try:
        proto_name = proto.name
        #tracks[proto_name].warp_paths = tracks_spleeter[proto_name].warp_paths
        print('---> using pre-found proto: ' + proto_name)
        generate_paths_threaded(proto_name)
    except:
        proto_name = generate_paths_threaded(proto_candidate)
        print('found proto: ' + proto_name)
        if proto_name != proto_candidate:
            generate_paths_threaded(proto_name)
    end = time.time()
    
    print('init_instrumentals_threaded took: ' + str(end - start) + '\n--------------\n')
    return proto_name
        
# bin according to closest value in leftest value behind comma
# TRY: bin according to numbers of zeroes
# return median of largest bin
def get_largest_bin_from_row(row):
    row_sorted = np.sort(row)
    # amount of zeroes after the comma of the decimals 
    zero_amounts = num_zeros(row_sorted)
    row_rounded = []
    #print('rounded row: ' + str(row_rounded))
    for i, amount in enumerate(zero_amounts):
        row_rounded.append(round(row_sorted[i], int(amount) + 1))
    #print('rounded row: ' + str(row_rounded))

    bins = dict()
    for j, val in enumerate(row_rounded):
        if not val in bins.keys():
            bins[val] = [row_sorted[j]]
        else:
            bins[val].append(row_sorted[j])
    #print('BINS: ' + str(bins))

    # get largest bin
    largest_bin = []
    for k in bins:
        if len(bins[k]) > len(largest_bin):
            largest_bin = bins[k]

    med_bin = np.median(largest_bin)
    #print('largest_bin: ' + str(largest_bin) + ' - median: ' + str(med_bin))
    return med_bin

def get_largest_bin(to_compare):
    result_vec = []
    for i, row in enumerate(to_compare):
        # print('checking bin for row: ' + str(len(to_compare) - i))
        result_vec.append(get_largest_bin_from_row(row))

    return np.asarray(result_vec)


def num_zeros(decimal_vec):
    return np.floor(np.abs(np.log10(decimal_vec)))


''' returns a vector whos values are the closest value from each row to the corresponding row of the reference vector '''
def get_closest_np(arr, vec):
    abses = np.abs(arr.T - vec).T
    mins_coords = abses.argmin(axis=1)
#print(abses)
    indexlist = [[i,coord] for i, coord in enumerate(mins_coords)]
    indexlisttranspose = np.array(indexlist).T.tolist()
    #print ('indexlist.T:', indexlisttranspose)
    #print ('y[indexlist.T]:', A[ tuple(indexlisttranspose) ])
    return arr[ tuple(indexlisttranspose) ]


def final_vector_from_block(compare_block, use):

    to_compare_spec_L = compare_block[0][0]
    to_compare_spec_R = compare_block[0][1]

    if use == 'closest' and len(instrumental_spec_L) != 0:
        if compare_block[1] != None:
            instrumental_vec_L = compare_block[1][0]
            instrumental_vec_R = compare_block[1][1]


            result_vec_spec_L = get_closest_np(to_compare_spec_L.T, instrumental_vec_L)
            result_vec_spec_R = get_closest_np(to_compare_spec_R.T, instrumental_vec_R)
        else:
            result_vec_spec_L = to_compare_spec_L[0]
            result_vec_spec_R = to_compare_spec_R[0]

        #instrumental_vec_coordinate = proto.warp_paths[folder_name][i][1]
        #instrumental_vec_L = instrumental_spec_L.T[instrumental_vec_coordinate]
        #instrumental_vec_R = instrumental_spec_R.T[instrumental_vec_coordinate]
    
        #for num in [1, 100, 101, 102, 103, 200, 201, 1000, 1001]:
        #df_L.T[1].plot()
        # df_L.T[2].plot()
        # df_L.T[3].plot()
        # df_L.T[100].plot()
        # df_L.T[101].plot()
        # df_L.T[102].plot()
        #plot_candidates(df_L[0])
        #groundtruth_outliers_total_L += len(desc_L[desc_L['max'] < desc_L['INSTR']]) + len(desc_L[desc_L['min'] > desc_L['INSTR']])


        #if (i == int(len(proto.specto_T) / 4)) or (i == int(len(proto.specto_T) / 3)) or (i == int(len(proto.specto_T) / 2)) or (i == int(len(proto.specto_T) - 1 )):

    elif use == 'min':
        #result_vec_spec = np.min(to_compare_spec, axis=0)
        result_vec_spec_L = np.min(to_compare_spec_L, axis=0)
        result_vec_spec_R = np.min(to_compare_spec_R, axis=0)
    elif use == 'max':
        #result_vec_spec = np.max(to_compare_spec, axis=0)
        result_vec_spec_L = np.max(to_compare_spec_L, axis=0)
        result_vec_spec_R = np.max(to_compare_spec_R, axis=0)
    elif use == 'median':
        #result_vec_spec = np.median(to_compare_spec, axis=0)
        result_vec_spec_L = np.median(to_compare_spec_L, axis=0)
        result_vec_spec_R = np.median(to_compare_spec_R, axis=0)
    elif use == 'bins':
        #result_vec_spec = get_largest_bin(to_compare_spec.T)
        result_vec_spec_L = get_largest_bin(to_compare_spec_L.T)
        result_vec_spec_R = get_largest_bin(to_compare_spec_R.T)
    elif use == 'mean':
        #result_vec_spec = np.mean(to_compare_spec, axis=0)
        result_vec_spec_L = np.mean(to_compare_spec_L, axis=0)
        result_vec_spec_R = np.mean(to_compare_spec_R, axis=0)
    elif use == 'mode':
        #result_vec_spec = np.apply_along_axis(lambda x: np.bincount(x).argmax(), axis=0, arr=to_compare_spec)
        
        #result_vec_spec = np.asarray([stats.mode(row)[0][0] for row in to_compare_spec.T])#, order='F')
        result_vec_spec_L = np.asarray([stats.mode(row)[0][0] for row in to_compare_spec_L.T])#, order='F')
        result_vec_spec_R = np.asarray([stats.mode(row)[0][0] for row in to_compare_spec_R.T])#, order='F')

    #print('shape of result_vec_spec: ' + str(result_vec_spec.shape))
    #print('beginning of result_vec_spec: ' + str(result_vec_spec[:20]))
    
    # start final specto with first vector (transpose from row to col)
    #if i == 0:
        #final_spectogram = result_vec_spec.T
    #    final_spectogram_L = result_vec_spec_L.T
    #    final_spectogram_R = result_vec_spec_R.T
    #else:
        #final_spectogram = np.column_stack((final_spectogram, result_vec_spec.T))
     #   final_spectogram_L = np.column_stack((final_spectogram_L, result_vec_spec_L.T))
     #   final_spectogram_R = np.column_stack((final_spectogram_R, result_vec_spec_R.T))

    return (result_vec_spec_L, result_vec_spec_R)

# function that gets the corresponding vectors of the passed protos paths at position i
def group_matching_vectors_at(i, proto, variants=[]):
    if i >= len(proto.specto_L):
        return [], [], [], [], []
    to_compare_spec_L = np.asarray([ proto.specto_L[i] ])
    to_compare_spec_R = np.asarray([ proto.specto_R[i] ])
    to_compare_phase_L = np.asarray([ proto.phase_L[i] ])
    to_compare_phase_R = np.asarray([ proto.phase_R[i] ])
    instrumental_vecs = None

    try:
        instr_warp_path = proto.warp_paths[folder_name]
        instr_offset = proto.offsets[folder_name]
        #if i < len(instr_warp_path):
        if i < np.shape(tracks[folder_name].specto_R)[0]:
            instrumental_vec_coordinate = i + instr_offset#instr_warp_path[i][1]
            instrumental_vecs = [instrumental_spec_L.T[instrumental_vec_coordinate], instrumental_spec_R.T[instrumental_vec_coordinate]]
    except:
        # print('error with instrumental coordinates number ' + str(i))
        pass

    # find all interesting chroma/specto vectors and put them in to_compare
    for track_name in proto.warp_paths:
        #print('comparing paths:' + proto.name + ' and ' + track_name)
        # skip the instrumental (which has folder name)
        if track_name == folder_name:
            continue

        cur_spec_L = tracks[track_name].specto_L
        cur_spec_R = tracks[track_name].specto_R
        cur_offset = proto.offsets[track_name]
        cur_phase_L = tracks[track_name].phase_L
        cur_phase_R = tracks[track_name].phase_R

        # if they still overlap
        #if len(proto.warp_paths[track_name]) > i:
            #coordinate = proto.warp_paths[track_name][i]
        cur_index = i + cur_offset
        #if np.shape(tracks[track_name].specto_L)[1] >= cur_index:
            
        if cur_index < len(cur_spec_L):
            vec_spec_L = cur_spec_L[cur_index]
            vec_spec_R = cur_spec_R[cur_index]
            vec_phase_L = cur_phase_L[cur_index]
            vec_phase_R = cur_phase_R[cur_index]

            # if both spectograms have the same amount of bins (which they should)
            if len(cur_spec_L[cur_index]) == len(to_compare_spec_L[0]):
                to_compare_spec_L = np.row_stack((to_compare_spec_L, vec_spec_L))
                to_compare_spec_R = np.row_stack((to_compare_spec_R, vec_spec_R))
                to_compare_phase_L = np.row_stack((to_compare_phase_L, vec_phase_L))
                to_compare_phase_R = np.row_stack((to_compare_phase_R, vec_phase_R))
                for variant in variants:
                    to_compare_spec_L = np.row_stack((to_compare_spec_L, variant[track_name].specto_L[cur_index]))
                    to_compare_spec_R = np.row_stack((to_compare_spec_R, variant[track_name].specto_R[cur_index]))
                    to_compare_phase_L = np.row_stack((to_compare_phase_L, variant[track_name].phase_L[cur_index]))
                    to_compare_phase_R = np.row_stack((to_compare_phase_R, variant[track_name].phase_R[cur_index]))

                #to_compare_phase.append(vec_phase)
            else:
                print('vector length mismatch. not appending a vector to the matrix')
    return to_compare_spec_L, to_compare_spec_R, to_compare_phase_L, to_compare_phase_R, instrumental_vecs


# variants: additional variants to take into consideration, eg tracks_spleeted, tracks_demucs
def final_spectogram_from_proto(proto, use='median', variants=[]):
    print('comparing paths:' + proto.name )
    print(proto.name + 's total paths to compare: ' + str(len(proto.warp_paths)))
    global groundtruth_outliers_total_L
    global groundtruth_outliers_total_R
    groundtruth_outliers_total_L = 0
    groundtruth_outliers_total_R = 0
    final_spectograms = {}
    to_compare_all = []
    to_compare_all_phase = []
    print('building compare blocks for faster processing..')
    for i, vec in enumerate(tqdm(proto.specto)):
        if i < len(proto.specto):
            to_compare_spec_L, to_compare_spec_R, to_compare_phase_L, to_compare_phase_R, instrumental_vecs = group_matching_vectors_at(i, proto, variants)
            if len(to_compare_spec_L) == 0:
                break
            to_compare_all.append((np.asarray([to_compare_spec_L, to_compare_spec_R]), instrumental_vecs))
            to_compare_all_phase.append((np.asarray([to_compare_phase_L, to_compare_phase_R]), None))
    print('building compare blocks for faster processing.. done.')

        # compare phases also!?

    if '-threaded' in sys.argv:
        final_vec_with_use = partial(final_vector_from_block, use=use)
        #with multiprocess.Pool(os.cpu_count()) as pool:
        #    all_vecs = pool.map(final_vec_with_use, to_compare_all)

        #for cs in [4, 8, 16, 32, 64, 256, 1024]:
        final_spectogram_L = []
        final_spectogram_R = []
        final_phase_L = []
        final_phase_R = []

        start = time.time()
        all_vecs = process_map(final_vec_with_use, to_compare_all, max_workers=os.cpu_count(), chunksize=4)#cs)
        all_vecs_phase = process_map(final_vec_with_use, to_compare_all_phase, max_workers=os.cpu_count(), chunksize=4)#cs)

        for i, pair in enumerate(all_vecs):
            if i == 0:
                final_spectogram_L = pair[0]#.T
                final_spectogram_R = pair[1]#.T
                final_phase_L = all_vecs_phase[i][0]
                final_phase_R = all_vecs_phase[i][1]
            else:
                final_spectogram_L = np.row_stack((final_spectogram_L, pair[0]))
                final_spectogram_R = np.row_stack((final_spectogram_R, pair[1]))
                final_phase_L = np.row_stack((final_phase_L, all_vecs_phase[i][0]))
                final_phase_R = np.row_stack((final_phase_R, all_vecs_phase[i][1]))

        mid = time.time()
        process_map_time = str(mid - start)
        print('process map with chunksize 4 took: ' + process_map_time)
        #print('process map with chunksize: ' + str(cs) + ' took: ' + process_map_time)
    else:
        mid = time.time()
        final_spectogram_L = None
        final_spectogram_R = None
        final_phase_L = []
        final_phase_R = []
        
        for i, compare_block in enumerate(to_compare_all):
            vecs = final_vector_from_block(compare_block, use=use)#final_phase.append(result_vec_phase)
            phases = final_vector_from_block(to_compare_all_phase[i] , use=use)#final_phase.append(result_vec_phase)
            if i == 0:
                final_spectogram_L = vecs[0]#.T
                final_spectogram_R = vecs[1]#.T
                final_phase_L = phases[0]
                final_phase_R = phases[1]
            else:
                #final_spectogram_L = np.column_stack((final_spectogram_L, vecs[0].T))
                #final_spectogram_R = np.column_stack((final_spectogram_R, vecs[1].T))
                final_spectogram_L = np.row_stack((final_spectogram_L, vecs[0]))
                final_spectogram_R = np.row_stack((final_spectogram_R, vecs[1]))
                final_phase_L = np.row_stack((final_phase_L, phases[0]))
                final_phase_R = np.row_stack((final_phase_R, phases[1]))
            #if i in [1,2,3,100,101,102,201,202,203]:
            #    plot_candidates(compare_block[0][0], compare_block[1][0])
        end = time.time()
        print('for loop took: ' + str(end - mid))
    
    #print('percentage of outliers of ground truth : ' + str(groundtruth_outliers_total / len(proto.specto_T)) + ' %')
    return {'L': final_spectogram_L.T, 'R': final_spectogram_R.T, 'phase_L': final_phase_L.T, 'phase_R': final_phase_R.T} 

def write_audio(final_spectogram, text='', final_phases=None):
    if len(final_spectogram) == 2:
        final_L = final_spectogram[0]
        final_R = final_spectogram[1]
        _init = 'random'
        if final_phases != None:
            _init = None
            ## make complex 
            #final_L = np.array(final_spectogram[0], dtype=complex)
            #final_R = np.array(final_spectogram[1], dtype=complex)
            ## Now define the imaginary part:
            #final_L.imag = final_phases[0]
            #final_R.imag = final_phases[1]
            stftMatrix_L = final_spectogram[0] * np.exp(final_phases[0] * 1j) # magnitude * e^(j*phase)
            stftMatrix_R = final_spectogram[1] * np.exp(final_phases[1] * 1j) # magnitude * e^(j*phase)
            if '-griffin' in sys.argv:
                _S_L = librosa.griffinlim(stftMatrix_L, n_iter=100, momentum=mom, hop_length=hop_length, init=_init)
                _S_R = librosa.griffinlim(stftMatrix_R, n_iter=100, momentum=mom, hop_length=hop_length, init=_init)
                text += ' griffin'
            else:
                _S_L = librosa.istft(stftMatrix_L, hop_length=hop_length)
                _S_R = librosa.istft(stftMatrix_R, hop_length=hop_length)
        else:
            print('using griffinlim to generate stereo audio for ' + text)
            mom = 0.05
            _S_L = librosa.griffinlim(final_L, n_iter=100, momentum=mom, hop_length=hop_length, init=_init)
            _S_R = librosa.griffinlim(final_R, n_iter=100, momentum=mom, hop_length=hop_length, init=_init)

        if '-normalize' in sys.argv:
            print('normalizing result for ' + text)
            _S_L = normalize(_S_L)
            _S_R = normalize(_S_R)
        name = 'out' + text
        sf.write(os.path.join(path_results, name + '.wav'), np.column_stack((_S_L, _S_R)), proto.sr, subtype="PCM_16")

        fig_out_L = plot_specto(final_spectogram[0], 'Channel L Spectogram: ' + text, proto.sr)#, 512)
        fig_out_L.savefig(os.path.join(path_results, name + '_L.png'))
        if not SHOW_FIG:
            plt.close(fig_out_L)

        fig_out_R = plot_specto(final_spectogram[1], 'Channel R Spectogram: ' + text, proto.sr)#, 512)
        fig_out_R.savefig(os.path.join(path_results, name + '_R.png'))
        if not SHOW_FIG:
            plt.close(fig_out_R)

    else:
        print('generating mono audio for ' + text)
        _S = librosa.griffinlim(final_spectogram, n_iter=100, momentum=0.1, hop_length=hop_length)
        if '-normalize' in sys.argv:
            _S = normalize(_S)
        name = 'out' + text
        sf.write(os.path.join(path_results, name + '.wav'), _S, proto.sr, subtype="PCM_24")

        fig_out = plot_specto(final_spectogram, 'Mono Spectogram: ' + text, proto.sr)#, 512)
        fig_out.savefig(os.path.join(path_results, name + '.png'))
        if not SHOW_FIG:
            plt.close(fig_out)


def squared_distance_of_specs(spec_ref, spec_estim, path):
    score = 0

    for coord in path:
        for r, row in enumerate(spec_ref[coord[0]]):
            val_estim = spec_estim[coord[1]][r]
            score += (val_estim - row) ** 2

    print(score)
    return score

def get_heatmap(spec_ref, spec_estim, path, mode='square'):
    first = True
    for (coord_estim, coord_ref) in path:
        if coord_estim < len(spec_estim.T) and coord_ref < len(spec_ref.T):
            if mode == 'square':
                # actually a column of a frame, but transposed
                row = np.square(spec_estim.T[coord_estim] - spec_ref.T[coord_ref])
            elif mode == 'abs':
                row = np.abs(spec_estim.T[coord_estim] - spec_ref.T[coord_ref])
            elif mode == 'subtract':
                row = spec_estim.T[coord_estim] - spec_ref.T[coord_ref]

            if first:
                first = False
                heatmap = np.asarray([row])
            else:
                heatmap = np.row_stack((heatmap, row))
    print('heatmap dims: ' + str(heatmap.shape))
    return heatmap.T

def path_trimmed(path, length):
    trimmed = []
    for p in path:
        if p[0] < length and p[1] < length:
            trimmed.append(p)
    return np.asarray(trimmed)


def heatmap_score(heatmap):
    return np.sum(np.abs(heatmap))


def compare_with_truth(spec, truth, channel):
    # get graph with 'heat map' of squared distance error
    # take vector with highest squared or absolute distance of the reference from each channel of spec and visualise
    if '-normalize' in sys.argv:
        channel += '_normalized'
    path = path_trimmed(proto.warp_paths[folder_name], len(truth.T))

    mel_truth = librosa.feature.melspectrogram(S=truth)
    mel_spec = librosa.feature.melspectrogram(S=spec)

    heatmap_square_mel = get_heatmap(mel_truth, mel_spec, path)
    heatmap_square = get_heatmap(truth, spec, path)
    fig_out = plot_specto(heatmap_square, 'channel: ' + channel + ' heatmap : (estimate - reference)**2 - score: ' + str(heatmap_score(heatmap_square)), proto.sr)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_square.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    fig_out = plot_specto(heatmap_square, 'channel: ' + channel + ' mel scale heatmap : (estimate - reference)**2 - score: ' + str(heatmap_score(heatmap_square_mel)), proto.sr)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_square_mel.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    heatmap_abs = get_heatmap(truth, spec, path, 'abs')
    fig_out = plot_specto(heatmap_abs, 'channel: ' + channel + ' heatmap : abs(estimate - reference) - score: ' + str(heatmap_score(heatmap_abs)), proto.sr)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_abs.png'))
    if not SHOW_FIG:
        plt.close(fig_out)
    heatmap_abs_mel = get_heatmap(mel_truth, mel_spec, path, 'abs')
    fig_out = plot_specto(heatmap_abs_mel, 'channel: ' + channel + ' mel scale heatmap : abs(estimate - reference) - score: ' + str(heatmap_score(heatmap_abs_mel)), proto.sr)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_abs_mel.png'))
    if not SHOW_FIG:
        plt.close(fig_out)
    
    heatmap_mel = get_heatmap(mel_truth, mel_spec, path, 'subtract')
    heatmap = get_heatmap(truth, spec, path, 'subtract')
    fig_out = plot_specto(heatmap, 'channel: ' + channel + ' heatmap : (estimate - reference) - score: ' + str(heatmap_score(heatmap)), proto.sr, to_db=False)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_subtr.png'))
    if not SHOW_FIG:
        plt.close(fig_out)
    fig_out = plot_specto(heatmap, 'channel: ' + channel + ' mel scale heatmap : (estimate - reference) - score: ' + str(heatmap_score(heatmap_mel)), proto.sr, to_db=False)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_subtr_mel.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    heatmap_sum = np.sum(heatmap, axis=0)
    heatmap_max_id = np.where(heatmap_sum == np.amax(heatmap_sum))[0][0]
    heatmap_min_id = np.where(heatmap_sum == np.amax(heatmap_sum))[0][0]
    heatmap_max_id = int(len(heatmap.T)/2)
    heatmap_max_id_truth = None
    heatmap_min_id_truth = None
    # find the truth vector of that maximum error id
    for p in path:
        if p[0] == heatmap_max_id and len(truth.T) > p[1]:
            heatmap_max_id_truth = truth.T[p[1]]
        elif p[0] == heatmap_min_id and len(truth.T) > p[1]:
            heatmap_min_id_truth = truth.T[p[1]]

    
    # find interesting area and the values of other tracks
    to_compare_spec_L, to_compare_spec_R, to_compare_phase_L, to_compare_phase_R, instrumental_vecs = group_matching_vectors_at(heatmap_max_id, proto)

    if channel.startswith('L'):
        foi = to_compare_spec_L
    elif channel.startswith('R'):
        foi = to_compare_spec_R

    fig_out = plot_candidates(foi, heatmap_max_id_truth, max_id=heatmap_max_id)
    fig_out.savefig(os.path.join(path_comparison, 'insight_' + channel + '.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    fig_out = plot_candidates(foi[:, 40:60], heatmap_max_id_truth[40:60], max_id=heatmap_max_id, _range=(40, 60))
    fig_out.savefig(os.path.join(path_comparison, 'insight_' + channel + '_40-60.png'))
    if not SHOW_FIG:
        plt.close(fig_out)
#    dist_l = squared_distance_of_specs(instrumental_spec_l, spec[0], path)
#    dist_r = squared_distance_of_specs(instrumental_spec_r, spec[1], path)
    

def make_audio(to_append='', variants=[]):

    global proto
    final_spectogram = final_spectogram_from_proto(proto, variants=variants)
    stereo = [final_spectogram['L'], final_spectogram['R']]
    #stereo_phase = [final_spectogram['phase_L'], final_spectogram['phase_R']]
    stereo_phase = [proto.phase_L.T, proto.phase_R.T]
    #stereo = np.asarray([final_spectogram['l'], final_spectogram['r']])
    result_instrumentals = stereo
    if len(instrumental_spec_L) > 0:
        compare_with_truth(final_spectogram['L'], instrumental_spec_L, 'L' + to_append)
        compare_with_truth(final_spectogram['R'], instrumental_spec_R, 'R' + to_append)
    for v in variants:
        to_append += '_' + v['_name']
    write_audio(stereo, '_median' + to_append, stereo_phase)
    
    if '-all-methods' in sys.argv or '-closest' in sys.argv:
        final_spectogram = final_spectogram_from_proto(proto, use='closest', variants=variants)
        #import ipdb; ipdb.set_trace(context=11)
        stereo = [final_spectogram['L'], final_spectogram['R']]
        write_audio(stereo, '_closest' + to_append)

    if '-all-methods' in sys.argv or '-bins' in sys.argv:
        final_spectogram_bins = final_spectogram_from_proto(proto, use='bins', variants=variants)
        stereo = np.asarray([final_spectogram_bins['L'], final_spectogram_bins['R']])
        write_audio(stereo, '_bins' + to_append)

    if '-all-methods' in sys.argv or '-mode' in sys.argv:
        final_spectogram_mode = final_spectogram_from_proto(proto, use='mode', variants=variants)
        stereo = np.asarray([final_spectogram_mode['L'], final_spectogram_mode['R']])
        write_audio(stereo, '_mode' + to_append)

    if '-all-methods' in sys.argv or '-max' in sys.argv:
        final_spectogram_max = final_spectogram_from_proto(proto, use='max', variants=variants)
        stereo = np.asarray([final_spectogram_max['L'], final_spectogram_max['R']])
        write_audio(stereo, '_max' + to_append)

def write_references(to_append=""):
    stereo_proto = [proto.specto_L.T, proto.specto_R.T]
    stereo_proto_phase = [proto.phase_L.T, proto.phase_R.T]
    write_audio(stereo_proto, '_proto' + to_append, stereo_proto_phase)
    stereo_instr = [instrumental_spec_L, instrumental_spec_R]
    stereo_instr_phase = [tracks[folder_name].phase_L.T, tracks[folder_name].phase_R.T]
    if '-debug' in sys.argv:
        print(len(stereo_instr))
        print(len(stereo_instr[0]))
    write_audio(stereo_instr, '_truth' + to_append, stereo_instr_phase)

def process_instrumentals(path_tracks, desc, variants=[]):
    #path_tracks = os.path.join(path_split_demucs, 'mdx_extra')
    #name = '_demucs_'

    if '-threaded' in sys.argv:
        proto_name = init_instrumentals_threaded(path_tracks)
    else:
        proto_name = init_instrumentals(path_tracks)
        #print('proto Name: ' + proto_name)
        #print('outside: ' + str(tracks))
    global proto
    proto = tracks[proto_name]
    init_truth()
    write_references(desc)
    make_audio(desc, variants)


def init_truth():
    global instrumental_spec_L
    global instrumental_spec_R
    instrumental_spec_L = tracks[folder_name].specto_L.T if folder_name in tracks.keys() else []
    instrumental_spec_R = tracks[folder_name].specto_R.T if folder_name in tracks.keys() else []

if __name__ == '__main__':
    tracks_demucs = {'_name': 'demucs'}
    tracks_spleeter = {'_name': 'spleeter'}
    tracks_raw = {'_name': 'raw tracks'}
    global tracks

    if '-paths' in sys.argv:
        REGENERATE_PATHS = True
    if '-spectos' in sys.argv:
        REGENERATE_SPECTOS = True

#    for _path in [path_split, path_split_16]:
    # if no spleeted files yet then separate
    proto = None
    if '-separate' in sys.argv or '-separate-only' in sys.argv:
        #mode = 'spleeter' if '-spleeted' in sys.argv else 'demucs'
        separate(track_folder_path, mode='demucs')
        separate(track_folder_path, mode='spleeter')
        print('SEPARATION DONE!!')
        if "-separate-only" in sys.argv:
            sys.exit()
    
    #proto_name = init_instrumentals()

    #proto_name = init_instrumentals_threaded(path_split)
    #proto = tracks[proto_name]
    #end = time.time()
    #print('-------BENCHMARK: normal took: ' + str(mid - start) + ' - threaded: ' + str(end - mid))
    print('------ACCOMPANIMENT RESULTS COMPARISON STARTED.')
    start = time.time()

    if '-spleeter' in sys.argv or '-all' in sys.argv:
        tracks = tracks_spleeter
        process_instrumentals(path_split_16, '_spleeter')
        #tracks_spleeter = tracks
    if '-demucs' in sys.argv or '-all' in sys.argv:
        tracks = tracks_demucs
        path_tracks = os.path.join(path_split_demucs, 'mdx_extra')
        process_instrumentals(path_tracks, '_demucs')
        if '-spleeter' in sys.argv or '-all' in sys.argv:
            process_instrumentals(path_tracks, '_demucs', [tracks_spleeter])
        tracks_demucs = tracks

    end = time.time()
    print('------ACCOMPANIMENT RESULTS COMPARISON DONE. TOOK: ' + str(end - start))


    if '-all' in sys.argv or '-tracks' in sys.argv:
        print('------TRACK COMPARISON STARTED.')
        start = time.time()
        tracks = tracks_raw
        if '-threaded' in sys.argv:
            proto_name = init_tracks_threaded()
        else:
            proto_name = init_tracks()
        proto = tracks[proto_name]
        '''
        final_spectograms = final_spectogram_from_proto(proto, use='max')
        stereo = np.asarray([final_spectograms['L'], final_spectograms['R']])
        write_audio(stereo, '_tracks_max')
        '''
        final_spectograms = final_spectogram_from_proto(proto, use='min')
        init_truth()
        write_references()
        if len(instrumental_spec_L) > 0:
            compare_with_truth(final_spectograms['L'], instrumental_spec_L, 'L_tracks')
            compare_with_truth(final_spectograms['R'], instrumental_spec_R, 'R_tracks')
        stereo = np.asarray([final_spectograms['L'], final_spectograms['R']])
        try:
            proto_phase_L = tracks_demucs[proto_name].phase_L
            proto_phase_R = tracks_demucs[proto_name].phase_R
        except:
            proto_phase_L = tracks[proto_name].phase_L
            proto_phase_R = tracks[proto_name].phase_R

        result_tracks = stereo
        stereo_phase = [proto_phase_L.T, proto_phase_R.T]
        write_audio(stereo, '_tracks', stereo_phase)

        if '-all' in sys.argv:
            final_spectograms_multi = final_spectogram_from_proto(proto, use='median', variants=[tracks_spleeter, tracks_demucs])
            stereo_multi = np.asarray([final_spectograms_multi['L'], final_spectograms_multi['R']])
            write_audio(stereo_multi, '_tracks_multi', stereo_phase)
        tracks = {}
        end = time.time()
        print('------TRACK COMPARISON DONE. TOOK: ' + str(end - start))

#Untersuchung zur Extraktion des Instrumentals aus mehreren Musikstücken mit gleichem Instrumental
