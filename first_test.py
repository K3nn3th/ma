#Untersuchung(/Vergleich) verschiedener Methoden zur Extraktion des Instrumentals aus Musikstücken mit gleichem Instrumental


#- Separierung mit Hilfe von spleeter -> anschl. Vergleich der Ergebnisse und "Korrektur"
# anh. von Wahrscheilichkeiten
#- Training mit instrumentals - vocals mix
#- Alignment/Warping of Instrumentals
import sys, os
from spleeter.separator import Separator
from spleeter.audio.adapter import get_default_audio_adapter

audio_loader = get_default_audio_adapter()

# Using embedded configuration.
separator = Separator('spleeter:2stems')

args = sys.argv[1:]
track_paths = []
for arg in args:
    #if any(['.mp3', '.flac'] in arg):
    track_paths.append(arg)

for track in track_paths:
    print('separating: ' + track)
    sample_rate = 44100
    waveform, _ = audio_loader.load(track, sample_rate=sample_rate)
    # Perform the separation :
    prediction = separator.separate(waveform)


print('All tracks separated')
