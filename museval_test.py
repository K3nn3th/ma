import mir_eval
import museval
import nussl
import librosa
import numpy as np
import pprint

path_estim = '/run/media/engineer/DATA/UNI/MA/data/kuff_again/results/out_median_demucs.wav'
path_ref = '/run/media/engineer/DATA/UNI/MA/data/kuff_again/results/out_truth_demucs.wav'

estim, sr = librosa.load(path_estim)
ref, sr = librosa.load(path_ref)

# assume they are aligned already.
# in future needs alignment for proper evaluation of tracks with the instrumental starting a little late
if len(estim) > len(ref):
    estim = estim[:len(ref)]
else:
    ref = ref[:len(estim)]

estim_nps = [] 
estim_nps.append(np.asarray(estim))

ref_nps = []
ref_nps.append(np.asarray(ref))

ref_nps = np.asarray(ref_nps)
estim_nps = np.asarray(estim_nps)
sdr, isr, sir, sar = museval.evaluate(ref_nps, estim_nps, mode="v3")
print('\nmus_eval results:')
#mus_dic = dict(mus_eva)
print('SDR')
pprint.pprint(sdr)
print('ISR')
pprint.pprint(isr)
print('SIR')
pprint.pprint(sir)
print('SAR')
pprint.pprint(sar)



def _report_sdr(approach, scores):
    SDR = {}
    SIR = {}
    SAR = {}
    print(approach)
    print(''.join(['-' for i in range(len(approach))]))
    for key in scores:
        if key not in ['combination', 'permutation']:
            SDR[key] = np.mean(scores[key]['SI-SDR'])
            SIR[key] = np.mean(scores[key]['SI-SIR'])
            SAR[key] = np.mean(scores[key]['SI-SAR'])
            print(f'{key} SI-SDR: {SDR[key]:.2f} dB')
            print(f'{key} SI-SIR: {SIR[key]:.2f} dB')
            print(f'{key} SI-SAR: {SAR[key]:.2f} dB')
            print()
    print()

ref_np_as = nussl.AudioSignal(path_ref)
estim_np_as = nussl.AudioSignal(path_estim)
bss = nussl.evaluation.BSSEvalScale(
    ref_np_as, estim_np_as)#,
#source_labels=['acc', 'vox'])
scores = bss.evaluate()
_report_sdr('Ideal Binary Mask', scores)



