import sys, os, museval, json
import mir_eval
import librosa
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.lines import Line2D
from matplotlib.colors import ListedColormap
import pprint
import nussl

from dtw import dtw
from fastdtw import fastdtw
from numpy.linalg import norm
from palettable.scientific.diverging import Berlin_20

from scipy.spatial.distance import euclidean

SHOW_FIG = True

n_fft = 2**10
hop_length = n_fft//4
seconds = 5
rate = 1


SMALL_SIZE = 16
MEDIUM_SIZE = 20
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

def evaulate_nussl(reference, estimated):
    pass


def evaluate_mir(reference, estimated):
    #valid = mir_eval.separation.validate(reference, estimated)
    mir_scores = mir_eval.separation.evaluate(reference, estimated)
    print('\nmir_eval results:')
    mir_dic = dict(mir_scores)
    pprint.pprint(mir_dic)

def evaluate_mus(reference, estimated):
    sdr, isr, sir, sar = museval.evaluate(reference, estimated, mode="v3")
    print('\nmus_eval results:')
    #mus_dic = dict(mus_eva)
    print('SDR')
    pprint.pprint(sdr)
    pprint.pprint(np.mean(sdr))
    print('ISR')
    pprint.pprint(isr)
    pprint.pprint(np.mean(isr))
    print('SIR')
    pprint.pprint(sir)
    pprint.pprint(np.mean(sir))
    print('SAR')
    pprint.pprint(sar)
    pprint.pprint(np.mean(sar))
    
def plot_signals(sig1, sig2):
    X = np.arange(0, 100000, 0.05)
    plt.plot(X, sig1, color='r', label='sig1')
    plt.plot(X, sig2, color='g', label='sig2')
      
# Naming the x-axis, y-axis and the whole graph
    plt.xlabel("Angle")
    plt.ylabel("Magnitude")
    plt.title("functions")
      
# Adding legend, which helps us recognize the curve according to it's color
    plt.legend()
      
# To load the display window
    plt.show()


def evaluate_null_test(ref, estim, path):
    plot_signals(ref, estim)
    score = 0

    for coord in path:
        val_ref = ref[coord[0]]
        val_estim = estim[coord[1]]
        score += (val_estim - val_ref) ** 2

    print(score)
    return score


def evaluate_squared_distance_acc(spec_ref, spec_estim, path):
    
    score = 0

    for coord in path:
        if coord[1] < len(spec_estim):
            for r, row in enumerate(spec_ref[coord[0]]):
                val_estim = spec_estim[coord[1]][r]
                score += (val_estim - row) ** 2

    print(score)
    return score

def get_best_fit_c(path, m = 1):
    x = path[:, 1]
    y = path[:, 0]

    c = -np.median(y - x)# - (m * np.mean(y))
    
    rounded = int(round(c))
    #print('choosing ' + str(rounded) + ' over ' + str(c))
    return rounded

def generate_path(ref_specto, estim_specto):
    
    # D and wp are numpy arrays
    D, wp = librosa.sequence.dtw(X=ref_specto, Y=estim_specto, metric='euclidean')
    #D, wp = librosa.sequence.dtw(X=ref_specto, Y=estim_specto, metric='cosine')

    c = get_best_fit_c(wp)
    #c = None
    wp_n = None


    # generating the line points
    if c != None:
        # take first instance of last value of wp as length value

        #print('First value pair of calculated path: ' + str(wp[0]))
        #print('Last x value of calculated path: ' + str(wp[-1]))

        highest_x = wp[0][1]
        wp_n = []
        for x in range(highest_x):
            if x >= 0 and x + c >= 0:
                wp_n.append((x, x + c))
    
    wp_n = np.asarray(wp_n)
    print('first part of path: ' + str(wp_n[:10]))
    return wp_n

def normalize(audio):
    max_peak = np.max(np.abs(audio))
    ratio = 1 / max_peak
    return audio * ratio


def get_heatmap(spec_ref, spec_estim, path, mode='square'):
    first = True
    for (coord_estim, coord_ref) in path:
        if coord_estim < len(spec_estim.T) and coord_ref < len(spec_ref.T):
            if mode == 'square':
                # actually a column of a frame, but transposed
                row = np.square(spec_estim.T[coord_estim] - spec_ref.T[coord_ref])
            elif mode == 'abs':
                row = np.abs(spec_estim.T[coord_estim] - spec_ref.T[coord_ref])
            elif mode == 'subtract':
                row = spec_estim.T[coord_estim] - spec_ref.T[coord_ref]

            if first:
                first = False
                heatmap = np.asarray([row])
            else:
                heatmap = np.row_stack((heatmap, row))
    print('heatmap dims: ' + str(heatmap.shape))
    return heatmap.T

def path_trimmed(path, length):
    trimmed = []
    for p in path:
        if p[0] < length and p[1] < length:
            trimmed.append(p)
    return np.asarray(trimmed)


def heatmap_score(heatmap):
    return np.sum(np.abs(heatmap))


def compare_with_truth(spec, truth, channel):
    # get graph with 'heat map' of squared distance error
    # take vector with highest squared or absolute distance of the reference from each channel of spec and visualise
    if '-normalize' in sys.argv:
        channel += '_normalized'
    path = path_trimmed(proto.warp_paths[folder_name], len(truth.T))

    mel_truth = librosa.feature.melspectrogram(S=truth)
    mel_spec = librosa.feature.melspectrogram(S=spec)

    heatmap_square_mel = get_heatmap(mel_truth, mel_spec, path)
    heatmap_square = get_heatmap(truth, spec, path)
    fig_out = plot_specto(heatmap_square, 'channel: ' + channel + ' heatmap : (estimate - reference)**2 - score: ' + str(heatmap_score(heatmap_square)), proto.sr, to_db=to_db)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_square.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    fig_out = plot_specto(heatmap_square, 'channel: ' + channel + ' mel scale heatmap : (estimate - reference)**2 - score: ' + str(heatmap_score(heatmap_square_mel)), proto.sr, to_db=to_db)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_square_mel.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    heatmap_abs = get_heatmap(truth, spec, path, 'abs')
    fig_out = plot_specto(heatmap_abs, 'channel: ' + channel + ' heatmap : abs(estimate - reference) - score: ' + str(heatmap_score(heatmap_abs)), proto.sr, to_db=to_db)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_abs.png'))
    if not SHOW_FIG:
        plt.close(fig_out)
    heatmap_abs_mel = get_heatmap(mel_truth, mel_spec, path, 'abs')
    fig_out = plot_specto(heatmap_abs_mel, 'channel: ' + channel + ' mel scale heatmap : abs(estimate - reference) - score: ' + str(heatmap_score(heatmap_abs_mel)), proto.sr, to_db=to_db)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_abs_mel.png'))
    if not SHOW_FIG:
        plt.close(fig_out)
    
    heatmap_mel = get_heatmap(mel_truth, mel_spec, path, 'subtract')
    heatmap = get_heatmap(truth, spec, path, 'subtract')
    fig_out = plot_specto(heatmap, 'channel: ' + channel + ' heatmap : (estimate - reference) - score: ' + str(heatmap_score(heatmap)), proto.sr, to_db=False)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_subtr.png'))
    if not SHOW_FIG:
        plt.close(fig_out)
    fig_out = plot_specto(heatmap, 'channel: ' + channel + ' mel scale heatmap : (estimate - reference) - score: ' + str(heatmap_score(heatmap_mel)), proto.sr, to_db=False)
    fig_out.savefig(os.path.join(path_comparison, 'heatmap_' + channel + '_subtr_mel.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    heatmap_sum = np.sum(heatmap, axis=0)
    heatmap_max_id = np.where(heatmap_sum == np.amax(heatmap_sum))[0][0]
    heatmap_max_id = int(len(heatmap.T)/2)
    heatmap_max_id_truth = None
    # find the truth vector of that maximum error id
    for p in path:
        if p[0] == heatmap_max_id and len(truth.T) > p[1]:
            heatmap_max_id_truth = truth.T[p[1]]
            break
    
    # find interesting area and the values of other tracks
    to_compare_spec_L, to_compare_spec_R, to_compare_phase_L, to_compare_phase_R, instrumental_vecs = group_matching_vectors_at(heatmap_max_id, proto)

    if channel.startswith('L'):
        foi = to_compare_spec_L
    elif channel.startswith('R'):
        foi = to_compare_spec_R

    fig_out = plot_candidates(foi, heatmap_max_id_truth, max_id=heatmap_max_id)
    fig_out.savefig(os.path.join(path_comparison, 'insight_' + channel + '.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    fig_out = plot_candidates(foi[:, 40:60], heatmap_max_id_truth[40:60], max_id=heatmap_max_id, _range=(40, 60))
    fig_out.savefig(os.path.join(path_comparison, 'insight_' + channel + '_40-60.png'))
    if not SHOW_FIG:
        plt.close(fig_out)
#    dist_l = squared_distance_of_specs(instrumental_spec_l, spec[0], path)
#    dist_r = squared_distance_of_specs(instrumental_spec_r, spec[1], path)


# returns the parts of the path with a certain slope
def only_slope(path, slope=1, min_length=20):
    path_only_slope = []
    #import ipdb; ipdb.set_trace(context=11)
    for i, coord in enumerate(path[min_length:]):
        diff_x = path[i][0] - path[i - min_length][0]# - path[i][0]
        diff_y = path[i][1] - path[i - min_length][1]# - path[i][1]
        if diff_x == min_length and diff_y == min_length * slope:
            path_only_slope.append(path[i - min_length])

    return np.asarray(path_only_slope)


def init_result(path):
    name = path.split('/')[-1]
    sig, sr = librosa.load(path)
    # sig = normalize(sig)
    spec = np.abs(librosa.stft(sig, n_fft=n_fft, hop_length=hop_length))
    mel = librosa.feature.melspectrogram(S=spec)
    mfcc = librosa.feature.mfcc(y=sig, sr=sr)   #Computing MFCC values
    tracks[name] = {}
    tracks[name]['sig'] = sig
    tracks[name]['audiosignal'] = nussl.AudioSignal(path)
    tracks[name]['sr'] = sr
    tracks[name]['spec'] = spec
    tracks[name]['mel'] = mel
    tracks[name]['mfcc'] = mfcc
    return tracks[name]

def write_scores(estim, _name, proto, truth):
    if _name == 'tracks':
        name = ''
    else:
        name = _name + '_'
        
    if proto:
        name = name + 'proto'
    if truth:
        name = name + 'truth'

    json_obj = {'riddim': folder_name,  'variant': name ,'L2': estim['score_square_avg'], 'L2_mel': estim['score_square_mel_avg'], 'dist_mfcc': estim['dist_mfcc'], 'dist_mel': estim['dist_mel'], 'si_sdr': estim['si_sdr']}

#    if '-norm' in sys.argv:
#        name = 'normalized_' + name
    with open(path_scores, 'a+') as f:
        f.write(json.dumps(json_obj) + '\n')


def compare_result(name, proto=False, truth=False, to_db=False):
    if name == 'tracks':
        key_estim = 'out_tracks.wav'
        if proto:
            key_estim = 'out_proto.wav'
        elif truth:
            key_estim = 'out_truth.wav'
        #key_ref = 'out_truth.wav'
    else:
        key_estim = 'out_median_' + name + '.wav'
        #key_ref = 'out_truth_' + name + '.wav'

        if proto:
            key_estim = 'out_proto_' + name + '.wav'
        elif truth:
            key_estim = 'out_truth_' + name + '.wav'
        
    key_ref = 'out_truth.wav'

    estim = tracks[key_estim]
    ref = tracks[key_ref]

    if to_db:
        max_amp_mel = max(np.max(estim['mel']), np.max(ref['mel']))
        estim_mel = librosa.amplitude_to_db(estim['mel'], ref=max_amp_mel)
        ref_mel = librosa.amplitude_to_db(ref['mel'], ref=max_amp_mel)
        estim_spec = librosa.amplitude_to_db(estim['spec'], ref=max_amp_mel)
        ref_spec = librosa.amplitude_to_db(ref['spec'], ref=max_amp_mel)
        key_estim += '_dB_'
    else:
        estim_mel = estim['mel']
        estim_spec = estim['spec']
        ref_spec = ref['spec']
        ref_mel = ref['mel']


    print('computing mfcc distance for result: ' + key_estim)
    dist_mfcc, cost, acc_cost, path_mfcc = dtw(estim['mfcc'].T, ref['mfcc'].T, dist=lambda x, y: norm(x - y, ord=1))
    dist_mel, path_mel = fastdtw(estim['mel'].T, ref['mel'].T, dist=euclidean, radius=4)

    only_slopes_mel = only_slope(path_mel)
    c = get_best_fit_c(only_slopes_mel)

    highest_x = only_slopes_mel[-1][1]
    path_mel_straight = []
    for x in range(highest_x):
        if x >= 0 and x + c >= 0:
            path_mel_straight.append((x, x + c))

    #plot_wp(None, np.asarray(path_mel), key_estim, key_ref, estim['sr'], np.asarray(path_mel_straight), only_slopes_mel, False, c)


    heatmap_square = get_heatmap(ref_spec, estim_spec, path_mel_straight, 'square')
    heatmap_diff = get_heatmap(ref_spec, estim_spec, path_mel_straight, 'subtract')

    heatmap_square_mel = get_heatmap(ref_mel, estim_mel, path_mel_straight, 'square')
    heatmap_diff_mel = get_heatmap(ref_mel, estim_mel, path_mel_straight, 'subtract')

    estim['dist_mfcc'] = dist_mfcc
    estim['dist_mel'] = dist_mel
    estim['score_square'] = heatmap_score(heatmap_square)
    estim['score_square_avg'] = estim['score_square'] / (len(estim['mel'] * len(estim['mel'][0])))
    estim['score_square_mel'] = heatmap_score(heatmap_square_mel)
    estim['score_square_mel_avg'] = estim['score_square_mel'] / (len(estim['mel'] * len(estim['mel'][0])))
    estim['si_sdr'] = get_si_sdr(estim['audiosignal'])

    print(name + ' dist mfcc: ' + str(dist_mfcc))
    print(name + ' dist mel: ' + str(dist_mel))
    print(name + ' hm score square: ' + str(estim['score_square']))
    print(name + ' hm score square avg: ' + str(estim['score_square_avg']))
    print(name + ' hm score mel square: ' + str(estim['score_square_mel']))
    print(name + ' hm score mel square avg: ' + str(estim['score_square_mel_avg']))

    if not to_db:
        write_scores(estim, name, proto, truth)

    fig_out = plot_specto(heatmap_square, key_estim + ' heatmap squared - L2 score: ' + str(estim['score_square_avg']), estim['sr'], square=True, to_db=to_db)
    fig_out.savefig(os.path.join(path_hm, key_estim + '_heatmap_squared.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    fig_out = plot_specto(heatmap_square_mel, key_estim + ' mel heatmap squared - L2 score: ' + str(estim['score_square_mel_avg']), estim['sr'], mel=True, square=True, to_db=to_db)
    fig_out.savefig(os.path.join(path_hm, key_estim + '_mel_heatmap_squared.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    fig_out = plot_specto(heatmap_diff, key_estim + ' heatmap difference', estim['sr'], diff=True, to_db=to_db)
    fig_out.savefig(os.path.join(path_hm, key_estim + '_heatmap_diff.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    fig_out = plot_specto(heatmap_diff_mel, key_estim + ' mel heatmap difference', estim['sr'], diff=True, mel=True, to_db=to_db)
    fig_out.savefig(os.path.join(path_hm, key_estim + '_mel_heatmap_diff.png'))
    if not SHOW_FIG:
        plt.close(fig_out)

    return heatmap_diff

    #evaluate_mus(np.asarray([ref['spec']]), np.asarray([estim['spec']]))
    #dist_squared = evaluate_squared_distance_acc(ref['spec'], estim['spec'], path_mel_straight)
    #dist_mel_squared = evaluate_squared_distance_acc(ref['mel'], estim['mel'], path_mel_straight)
    #print(name + ' hm score mel square: ' + str(estim['score_square_mel']))

def my_colormap():
    top = cm.get_cmap('Oranges_r', 128)
    bottom = cm.get_cmap('Blues', 128)

    newcolors = np.vstack((top(np.linspace(0, 1, 128)),
                           bottom(np.linspace(0, 1, 128))))
    return ListedColormap(newcolors, name='OrangeBlue')

def plot_specto(specto, name, sr, diff = False, mel=False, square=False, to_db=False):
    if to_db:
        unit = "energy"
    else:
        unit = "dB"
    fig = plt.figure(figsize=(10,10))
    # make pixels square
    ax = fig.add_subplot(111)
    if not diff:
        cmap = plt.get_cmap('magma')
        if mel == False:
            y_axis = 'linear' 
        else:
            y_axis = 'mel'
        im = librosa.display.specshow(specto, y_axis=y_axis, x_axis='frames', hop_length=hop_length, sr=sr, cmap=cmap)
        #im = librosa.display.specshow(specto, x_axis="time", y_axis='linear', hop_length=hop_length, sr=sr, cmap=cmap)
        if square:
            cb = fig.colorbar(im, ax=ax, format="%+2.f")
            cb.set_label(f'Squared Error in {unit}', rotation=270, labelpad=20)
        else:
            fig.colorbar(im, ax=ax, format=f"%+2.f {unit}")
    else:
        cmap = my_colormap()#plt.get_cmap('RdBu')
        #cmap = Berlin_20.mpl_colormap
        #ax.set_xlabel('Time (s)')
        #ax.set_ylabel('Frequency bins')
        #psm = ax.pcolormesh(specto, cmap=cmap, rasterized=True, vmin=-np.max(specto), vmax=np.max(specto))
        if mel == False:
            y_axis = 'linear' 
        else:
            y_axis = 'mel'
        max_abs = max(np.max(specto), abs(np.min(specto)))
        im = librosa.display.specshow(specto, y_axis=y_axis, x_axis='frames', hop_length=hop_length, sr=sr, cmap=cmap, vmin=-max_abs, vmax=max_abs)
        cb = fig.colorbar(im, ax=ax, format=f"%+2.f")
        cb.set_label(f'Error in {unit}', rotation=270, labelpad=20)
        #fig.colorbar(psm, ax=ax, format="%+2.f energy diff")
    #im = librosa.display.specshow(specto, x_axis="time", y_axis='log', hop_length=hop_length, sr=sr, cmap="magma")
    plt.title(name)
    #plt.yscale('linear')
    #plt.show()
    return fig


def plot_wp(D, wp, x_name, y_name, sr, wp_n, wp_slope, with_D = True, c=''):
    wp_s = wp#np.asarray(wp) * hop_length / sr
    wp_ns = wp_n#np.asarray(wp_n) * hop_length / sr
    wp_slope_s = wp_slope #np.asarray(wp_slope) * hop_length / sr
    #print('length wp: ' + str(len(wp)))
    #print('length wp_s: ' + str(len(wp_s)))
    
    fig = plt.figure(figsize=(12, 10))
    # make pixels square
    ax = fig.add_subplot(111)
    if with_D:
        librosa.display.specshow(D, x_axis='time', y_axis='time',
                                 cmap='magma', hop_length=hop_length, sr=sr, vmin=0, vmax=500)
        #imax = ax.imshow(D, cmap=plt.get_cmap('magma'),
        #                origin='lower', interpolation='nearest', aspect='equal')
        plt.colorbar()

    ax.plot(wp_s[:, 0], wp_s[:, 1], marker='.', color='r', ms=5, label='dtw result')
    ax.plot(wp_slope_s[:, 0], wp_slope_s[:, 1], marker='.', color='b', ms=2, label='dtw result - only slopes', linewidth=0)
    ax.plot(wp_ns[:, 0], wp_ns[:, 1], marker='.', color='g', ms=1, label='approximated')
    plt.yscale('linear')
    #print('length wp_n: ' + str(len(wp_n)))
    #print('length wp_ns: ' + str(len(wp_ns)))
    
    
    ax.set(xlabel=x_name + ' [windows]', ylabel=y_name + ' [windows]')
    plt.title(f'Warping Path for {x_name} and {y_name} - offset: {c}')
    return fig


def get_si_sdr(estim_as):
    ref_as = tracks['out_truth.wav']['audiosignal']
    bss = nussl.evaluation.BSSEvalScale(ref_as, estim_as)
    scores = bss.evaluate()

    #_report_sdr('Ideal Binary Mask', scores)
    si_sdr = np.mean(scores[os.path.join(path_results, 'out_truth.wav')]['SI-SDR'])
    print(f"final thing: {si_sdr}")
    return si_sdr




# base_path = os.path.abspath('/run/media/engineer/DATA/UNI/MA/data')
base_path = os.path.abspath('/media/DATA/MA/data')

riddims = os.listdir(base_path)
folder_name = ""
for a in sys.argv:
    if a in riddims:
        folder_name = a
if folder_name == "":
    print('no riddim mentioned. please pass a name of a riddim.')
    sys.exit()



tracks = {}

track_folder_path = os.path.join(base_path, folder_name)
path_results = os.path.join(track_folder_path, 'results')
if '-norm' in sys.argv:
    path_results = os.path.join(track_folder_path, os.path.join('results', 'normalized'))
path_hm = os.path.join(path_results, 'heatmaps')

if not os.path.exists(path_hm):
    os.mkdir(path_hm)

path_scores = os.path.join(path_results, 'scores')
if os.path.exists(path_scores):
    open(path_scores, 'w').close()

if __name__ == "__main__":
    print('loading results and references for riddim: ' + folder_name)

    baseline_top = None
    baseline_bottom = None

    for p in ['out_truth.wav', 'out_truth_spleeter.wav', 'out_truth_demucs.wav', 'out_median_demucs.wav', 'out_median_spleeter.wav', 'out_tracks.wav', 'out_proto_demucs.wav', 'out_proto_spleeter.wav', 'out_proto.wav']:
        path = os.path.join(path_results, p)
        track = init_result(path)

    print('loading results and references for riddim: ' + folder_name + ' -> Done.')

    heatmap_tracks = compare_result('tracks')
    compare_result('tracks', to_db=True)
    compare_result('tracks', proto=True)
    compare_result('tracks', truth=True)
    compare_result('tracks', proto=True, to_db=True)

    heatmap_spleeter = compare_result('spleeter')
    compare_result('spleeter', to_db=True)
    compare_result('spleeter', proto=True)
    compare_result('spleeter', proto=True, to_db=True)
    compare_result('spleeter', truth=True)
    compare_result('spleeter', truth=True, to_db=True)

    heatmap_demucs = compare_result('demucs')
    compare_result('demucs', to_db=True)
    compare_result('demucs', proto=True)
    compare_result('demucs', proto=True, to_db=True)
    compare_result('demucs', truth=True)
    compare_result('demucs', truth=True, to_db=True)


    #heatmap_mean = (heatmap_tracks + heatmap_spleeter + heatmap_demucs) / 3
    #fig_out = plot_specto(heatmap_mean, folder_name + ' mean of heatmaps', estim['sr'], diff=False, mel=False)
    #fig_out.savefig(os.path.join(path_results, folder_name + '_mean_heatmaps.png'))
    #if not SHOW_FIG:
    #    plt.close(fig_out)

    with open(os.path.join(path_results, 'heatmap_diff_tracks'), 'w+b') as f:
        np.save(f, heatmap_tracks)
    with open(os.path.join(path_results, 'heatmap_diff_spleeter'), 'w+b') as f:
        np.save(f, heatmap_spleeter)
    with open(os.path.join(path_results, 'heatmap_diff_demucs'), 'w+b') as f:
        np.save(f, heatmap_demucs)
