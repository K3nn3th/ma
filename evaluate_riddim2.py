# from https://github.com/d4r3topk/comparing-audio-files-python/blob/master/mfcc.py

import librosa
import librosa.display
import matplotlib.pyplot as plt
from dtw import dtw
from numpy.linalg import norm

seconds = 20
#tracks = ['/home/archm4n/Documents/UNI/ma/data/Aidia Riddim/split_16/04 Shine On You/accompaniment.wav',
#          '/home/archm4n/Documents/UNI/ma/data/Aidia Riddim/results/out_spleeted_16.wav',
#          '/home/archm4n/Documents/UNI/ma/data/Aidia Riddim/results/out_tracks_min.wav'
#          ]
tracks = ['/home/archm4n/Documents/UNI/Masterarbeit/survey_page/static/riddims/aidia/out_tracks_trimmed.wav',
          '/home/archm4n/Documents/UNI/Masterarbeit/survey_page/static/riddims/aidia/out_median_demucs_trimmed.wav',
          '/home/archm4n/Documents/UNI/Masterarbeit/survey_page/static/riddims/aidia/out_proto_demucs_trimmed.wav',
          '/home/archm4n/Documents/UNI/Masterarbeit/survey_page/static/riddims/aidia/out_proto_spleeter_trimmed.wav',
          '/home/archm4n/Documents/UNI/Masterarbeit/survey_page/static/riddims/aidia/out_median_spleeter_trimmed.wav',
          ]
#Loading audio files
y1, sr1 = librosa.load('/home/archm4n/Documents/UNI/Masterarbeit/survey_page/static/riddims/aidia/out_truth_trimmed.wav')
#y2, sr2 = librosa.load('/home/archm4n/Documents/UNI/ma/data/Aidia Riddim/Aidia Riddim.wav') 
#y3, sr3 = librosa.load('/home/archm4n/Documents/UNI/ma/data/Aidia Riddim/results/out_spleeted_16.wav') 
plt.subplot(1, 2, 1) 
mfcc1 = librosa.feature.mfcc(y1[:seconds],sr1)   #Computing MFCC values
librosa.display.specshow(mfcc1)

for track in tracks:
    y, sr = librosa.load(track) 
    seconds = 30 * sr1

#Showing multiple plots using subplot

    plt.subplot(1, 2, 2)
    mfcc = librosa.feature.mfcc(y[:seconds], sr)
    librosa.display.specshow(mfcc)

#dist, cost, path = dtw(mfcc1.T, mfcc2.T)
    dist, cost, acc_cost, path = dtw(mfcc1.T, mfcc.T, dist=lambda x, y: norm(x - y, ord=1))
    print("The normalized distance between truth and " + track,dist)   # 0 for similar audios 

    plt.imshow(cost.T, origin='lower', cmap=plt.get_cmap('gray'), interpolation='nearest')
    plt.plot(path[0], path[1], 'w')   #creating plot for DTW

    plt.show()  #To display the plots graphically


