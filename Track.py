import librosa
import numpy as np
from matplotlib import pyplot as plt



class Track:    
    def __init__(self, name, filepath):
        self.name = name
        self.filepath = filepath
        self.warp_paths = {}
        self.warp_paths_rough = {}
        self.ds = {}
        self.specto = None#self.generate_specto()
        self.phase = None
        self.total_curvature = None
        
    
    def generate_paths(self):
        start = time.time()
        total = 0
        for t in tracks:
            if t != self.name:
                D, wp = librosa.sequence.dtw(X=self.specto, Y=tracks[t].specto, metric='cosine')
                
                c = get_best_fit_c(wp)
                #c = None
                wp_n = None
                if c != None:
                    # calculate values and plot!
                    # take first instance of last value of wp as length value
                    
                    #print('First value pair of calculated path: ' + str(wp[0]))
                    #print('Last x value of calculated path: ' + str(wp[-1]))
                    
                    highest_x = wp[0][0]
                    wp_n = []
                    for x in range(highest_x):
                        wp_n.append((x , x + c))
                        
                plot_wp(D, wp, tracks[t].name, self.name, self.sr, wp_n=wp_n)
                           
                self.warp_paths_rough[t] = wp[::-1]
                self.warp_paths[t] = self.warp_paths_rough[t]
                if wp_n != None:
                    self.warp_paths[t] = wp_n[::-1]
                
                self.ds[t] = D                
                
                curv = get_curvature_for_path(self.warp_paths[t])
                print(t + 's curvature: ' + str(curv))
                total += curv
                
                print(self.name + ' generated path for: ' + t )
        self.total_curvature = total# / len(self.warp_paths)
        end = time.time()
        print('generating paths took: ' + str(end - start))
        print(self.name + ' generated ' + str(len(self.warp_paths)) + ' warp paths')
             
            
    # handle (-)inf case ?
    def get_all_curvatures(self):
        total = 0
        for path in self.warp_paths:
            c = get_curvature_for_path(self.warp_paths[path])
            print(path + 's curvature: ' + str(c))
            total += c
        self.total_curvature = total# / len(self.warp_paths)
        print(self.name + ' total_curvature: ' + str(self.total_curvature))        
        
    def smooth_paths(self, kernel_size):
        for p in self.warp_paths:
            #self.warp_path[p] = 
            D = 0
            plot_wp(self.ds[p], self.warp_paths[p], p, self.name, self.sr)
            new = smooth_path(self.warp_paths[p], kernel_size)
            plot_wp(self.ds[p], new, p + ' - smooth', self.name + ' - smooth', self.sr)#, False)

def plot_wp(D, wp, x_name, y_name, sr, with_D = True, wp_n=None):
    wp_s = np.asarray(wp) * hop_length / sr
    #print('length wp: ' + str(len(wp)))
    #print('length wp_s: ' + str(len(wp_s)))
    

    
    fig = plt.figure(figsize=(12, 10))
    # make pixels square
    ax = fig.add_subplot(111)
    if with_D:
        librosa.display.specshow(D, x_axis='time', y_axis='time',
                                 cmap='magma', hop_length=hop_length, sr=sr, vmin=0, vmax=500)
        imax = ax.imshow(D, cmap=plt.get_cmap('magma'),
                         origin='lower', interpolation='nearest', aspect='equal')
        plt.colorbar()
    if wp_n != None:
        wp_ns = np.asarray(wp_n) * hop_length / sr
        #print('length wp_n: ' + str(len(wp_n)))
        #print('length wp_ns: ' + str(len(wp_ns)))
        ax.plot(wp_ns[:, 1], wp_ns[:, 0], marker='o', color='g')
    ax.plot(wp_s[:, 1], wp_s[:, 0], marker='o', color='r')
    ax.set(xlabel=x_name, ylabel=y_name)
    plt.title('Warping Path on Acc. Cost Matrix $D$')
    
def test_plot_wp():
    plot_wp([],[(0, 0), (0, 1), (0, 2), (0, 3)])
            
