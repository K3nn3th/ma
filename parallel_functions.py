import os
import librosa
import numpy as np


n_fft = 2**11
hop_length = n_fft//4
# length of desired audio, None for entire track
seconds = 15

def get_curvatures(track):
    track.generate_paths()
    #self.smooth_paths(11)
    #track.get_all_curvatures()



def generate_specto(track):

    _fp = track.filepath#os.path.join(os.path.join(path_split, self.name), 'accompaniment.wav')
    #_fp = os.path.join(track_folder_path, self.name)

    if not os.path.exists(_fp):
        print(_fp + ' does not exist. please check')
        return None

    sig, sr = librosa.load(_fp, sr=None)
    print('loaded track/instrumental: ' + track.name + ' with sr=' + str(sr))
    track.sr = sr

    # trim silence at beginning and end to make it nicer
    sig, index = librosa.effects.trim(sig)

    #tuning = librosa.estimate_tuning(y=sig, sr=sr)
    #print(track_name + ' with tuning: ' + str(librosa.tuning_to_A4(tuning)))

    # DIFFERENT CHROMAGRAM / SPECTOGRAM GENERATION METHODS
    short = sig
    if seconds != None:
        short = sig[:sr * seconds]
    #specto = librosa.feature.chroma_stft(short, sr=sr, n_fft=n_fft, hop_length=hop_length)
    #specto = librosa.feature.chroma_cqt(sig, sr=sr, hop_length=hop_length)


    #print('shortened length: ' + str(len(short)))
    D = librosa.stft(short, n_fft=n_fft, hop_length=hop_length)
    specto = np.abs(D)
    phase = np.angle(D)
    '''
    D = librosa.stft():
    This function returns a complex-valued matrix D such that

    np.abs(D[f, t]) is the magnitude of frequency bin f at frame t, and

    np.angle(D[f, t]) is the phase of frequency bin f at frame t.

The integers t and f can be converted to physical units by means of the utility functions frames_to_sample and fft_frequencies.
    '''
    
    
    
    #specto = librosa.feature.melspectrogram(short, sr=sr)#, hop_length=hop_length)
    #y_inv = librosa.feature.inverse.mel_to_audio(specto, sr=sr)

    #display(ipd.Audio(y_inv, rate=sr))

    #specto_db = librosa.power_to_db(chroma, ref=np.max)
    #plot_chroma(specto_db, _fp, sr)
    prev_length = 0
    for vec in specto:
        if len(vec) != prev_length:
            print(track.name + ': old: ' + str(prev_length) + ' - new: ' + str(len(vec)))
            prev_length = len(vec)

    track.specto = specto
    track.phase = phase
