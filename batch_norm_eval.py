import sys, os

base = '/media/DATA/MA/data/'

for r in os.listdir(base):
    if os.path.isdir(os.path.join(base, r)):
        path_riddim = os.path.join(base, r)
        path_results = os.path.join(path_riddim, 'results/')
        os.chdir(path_results)
        print(f'evaluating output from {r}')
        os.system(f'python /home/archm4n/Documents/UNI/ma/evaluate_riddim.py {r}')
        #print(f'normalizing output from {r}')
        #os.system('python3.7 -m ffmpeg_normalize *.wav -t -12 -lrt 1.6 -tp -1.0 -ext wav -p -f')
        print(f'evaluating normalized output from {r}')
        os.system(f'python /home/archm4n/Documents/UNI/ma/evaluate_riddim.py -norm {r}')
    